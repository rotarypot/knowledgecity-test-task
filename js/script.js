
$(function () {


    // check for auth
    var session = sessionStorage.getItem('isTempAuthorized');
    var local = localStorage.getItem('isAuthorized');

    if (local || session != null) {
        window.location.href = "user-data.html"
    }

    $('#kc-form').submit(function (event) {
        event.preventDefault();
        var username = $('.username').val();
        var password = $('.password').val();
        var ls = window.localStorage;
        var ss = window.sessionStorage;

        if (username || password != '') {
            $.getJSON("auth.json", function (jsonData) {

                if (username == jsonData.username && password == jsonData.password) {

                    if ($('input:checkbox').is(":checked")) {
                        // remember auth 
                        ls.setItem('isAuthorized', true)

                    } else {
                        //do not remember auth
                        ss.setItem('isTempAuthorized', true)
                    }

                    window.location.replace('user-data.html')

                } else {
                    alert('login is incorrect')
                }
            })
        } else {
            alert('Complete the form fields')
        }
    })
})