$(function () {

    // check for auth
    var session = sessionStorage.getItem('isTempAuthorized');
    var local = localStorage.getItem('isAuthorized');

    if (local || session != null) {
        // if auth is valid, get data
        $.getJSON("studentdata.json", function (data) {
            makeTable(data)
        })

    }
    else {
        // if auth NOT valid, redirect to login
        window.location.href = 'index.html'
    }
})

function logout() {
    localStorage.removeItem('isAuthorized');
    sessionStorage.removeItem('isTempAuthorized');
    window.location.href = 'index.html'
}

function makeTable(data) {
    var container = $('div.table-container');
    var table = $("<table/>").addClass('table table-striped kc-table');
    var tbody = $("<tbody/>");

    $.each(data.students, function (rowIndex, student) {
        var row = $("<tr/>");
        $.each(student, function (colIndex, datum) {
            row.append($("<td/>").text(datum));
        });
        tbody.append(row);
    });
    table.append(tbody);
    return container.append(table);
}